const express = require('express');
const app = express();
const entryRoutes = require('./src/api-services/entry-routes')

app.use(entryRoutes); 

const port = 3000
const server = app.listen(port, () => console.log(`Server hosted in port ${port}`));