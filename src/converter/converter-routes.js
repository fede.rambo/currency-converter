const express = require('express');
const router = express.Router();
const { usdToPeso,pesoToUsd } = require('./converter-controller')


router
    .route('/peso-usd/:amount')
    .get(
        usdToPeso
    )

router
    .route('/usd-peso/:amount')
    .get(
        pesoToUsd
    )

module.exports = router
