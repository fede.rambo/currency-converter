const { connected } = require('process');

const axios = require('axios');

const usdToPeso = async (req,res) => {
    try{
        const connect = await axios.get('https://www.dolarsi.com/api/api.php?type=valoresprincipales');
        const pesos = parseInt(req.params.amount)
        const usd = parseFloat(connect.data[3].casa.compra)
        const result = pesos/usd
        return res.status(200).send({
            success: true,
            result
        })  
    }
    catch(error){
        console.error(error);
        res.status(400).send({
            success: false,
            message: 'failed to retrieve data from api'
        })
    }
}


const pesoToUsd = async (req,res) => {
    try{
        const connect = await axios.get('https://www.dolarsi.com/api/api.php?type=valoresprincipales');
        const usd = parseInt(req.params.amount)
        const pesos = parseFloat(connect.data[3].casa.compra)
        const result = usd*pesos
        return res.status(200).send({
            success: true,
            result
        })  
    }
    catch(error){
        console.error(error);
        res.status(400).send({
            success: false,
            message: 'failed to retrieve data from api'
        })
    }
}


module.exports = {
    usdToPeso,
    pesoToUsd
}