const express = require('express');
const converterRoutes = require('../converter/converter-routes');

const router = express.Router();

router.use('/convert', converterRoutes)

module.exports = router